# -*- coding: utf-8 -*-
from os.path import dirname, join
from urllib import urlopen

from werkzeug import escape, url_encode

import zine
from zine.api import *
from zine.widgets import Widget
from zine.views.admin import flash, render_admin_response
from zine.models import COMMENT_BLOCKED_SPAM, Comment
from zine.privileges import BLOG_ADMIN, require_privilege
from zine.utils.validators import ValidationError, check
from zine.utils.http import redirect_to
from zine.utils import forms

# Check do we have the recaptcha library installed

try:
    from recaptcha.client import captcha
    have_recaptcha = True
except ImportError:
    have_recaptcha = False

TEMPLATES = join(dirname(__file__), 'templates')

class ConfigurationForm(forms.Form):
    """The configuration form."""
    pubkey = forms.TextField()
    privkey = forms.TextField()


def do_spamcheck(req, comment):
    """Do spamchecking for all new comments."""
    # something blocked the comment already. no need to check for
    # spam then.
    if comment.blocked:
        return

    resp = captcha.submit(
        recaptcha_challenge_field=req.values['recaptcha_challenge_field'],
        recaptcha_response_field=req.values['recaptcha_response_field'],
        private_key=req.app.cfg['recaptcha_spam_filter/privkey'],
        remoteip=comment.submitter_ip
    )

    if resp.is_valid:
        # Captcha passed
        return
    else:
        comment.status = COMMENT_BLOCKED_SPAM
        comment.blocked_msg = "Blocked for failing the captcha"


def add_recaptcha_link(req, navigation_bar):
    """Add a button for recaptcha to the comments page."""
    if req.user.has_privilege(BLOG_ADMIN):
        for link_id, url, title, children in navigation_bar:
            if link_id == 'options':
                children.insert(-3, ('recaptcha_spam_filter',
                                     url_for('recaptcha_spam_filter/config'),
                                     _('reCAPTCHA')))


@require_privilege(BLOG_ADMIN)
def show_recaptcha_config(req):
    """Show the recaptcha control panel."""
    form = ConfigurationForm(initial=dict(
        pubkey=req.app.cfg['recaptcha_spam_filter/pubkey'],
        privkey=req.app.cfg['recaptcha_spam_filter/privkey']
    ))

    if req.method == 'POST' and form.validate(req.form):
        if form.has_changed:
            req.app.cfg.change_single('recaptcha_spam_filter/pubkey',
                                      form['pubkey'])
            req.app.cfg.change_single('recaptcha_spam_filter/privkey',
                                      form['privkey'])
            if form['pubkey']:
                flash(_('reCAPTCHA has been successfully enabled.'), 'ok')
            else:
                flash(_('reCAPTCHA disabled.'), 'ok')
        return redirect_to('recaptcha_spam_filter/config')
    return render_admin_response('admin/recaptcha_spam_filter.html',
                                 'options.recaptcha_spam_filter',
                                 form=form.as_widget())


def add_captcha(req, post):
    return captcha.displayhtml(req.app.cfg['recaptcha_spam_filter/pubkey'])

def setup(app, plugin):
    if not have_recaptcha:
        raise SetupError("The reCAPTCHA plugin requires the recaptcha python library")
    app.add_config_var('recaptcha_spam_filter/pubkey',
                       forms.TextField(default=u''))
    app.add_config_var('recaptcha_spam_filter/privkey',
                       forms.TextField(default=u''))
    app.add_url_rule('/options/recaptcha', prefix='admin',
                     endpoint='recaptcha_spam_filter/config',
                     view=show_recaptcha_config)
    app.connect_event('before-comment-editor-buttons-rendered', add_captcha)
    app.connect_event('before-comment-saved', do_spamcheck)
    app.connect_event('modify-admin-navigation-bar', add_recaptcha_link)
    app.add_template_searchpath(TEMPLATES)
